package com.mad.fight.menus.pause;

import com.mad.fight.DrawingContextProvider;
import com.mad.fight.components.Anchor;
import com.mad.fight.components.ImageAsset;
import com.mad.fight.components.Position;
import com.mad.fight.components.TextButton;
import com.mad.fight.menus.BlackBackground;
import com.mad.fight.menus.MenuScreenView;
import com.mad.fight.menus.pause.PauseController;
import javafx.scene.image.Image;
import javafx.scene.paint.Paint;

@SuppressWarnings("FieldCanBeLocal")
public class PauseView extends MenuScreenView {
    public PauseView(PauseController pauseController, DrawingContextProvider drawingContextProvider) {
        addDrawable((gc, updateCount, dc) -> {
            gc.setFill(Paint.valueOf("#fff4"));
            gc.fillRect(0, 0, dc.getScreenWidth(), dc.getScreenHeight());
        });

        addMouseComponent(new TextButton("resume game", Position.centeredX(70), drawingContextProvider).withAction(pauseController::resumeGame));

        addMouseComponent(new TextButton("quit", Position.centeredX(110), drawingContextProvider).withAction(pauseController::quit));
    }
}
