package com.mad.fight.menus.settings;

import com.mad.fight.DrawingContextProvider;
import com.mad.fight.components.*;
import com.mad.fight.menus.BlackBackground;
import com.mad.fight.menus.MenuScreenView;
import javafx.scene.image.Image;

public class SettingsView extends MenuScreenView {
    private static final Image background = new Image("png/lobby/background.png");

    public SettingsView(SettingsController settingsController, SettingsModel settingsModel, DrawingContextProvider drawingContextProvider) {
        addDrawable(new ImageAsset(background,new Position(Anchor.TOP_LEFT,false,false,0,0)));

        addDrawable(new Text("settings", Position.centeredX(10)));

        addDrawable(new Text("resolution", Position.centeredX(30)));
        addMouseComponent(new ResolutionSelector(Position.centeredX(40), settingsModel.getScalingFactor(), settingsModel::setScalingFactor, drawingContextProvider));

        addDrawable(new Text("sfx volume: ", Position.centeredX(60).withAnchor(Anchor.TOP_RIGHT)));
        addMouseComponent(new Slider(drawingContextProvider, Position.centeredX(60)).withOnValueChanged(settingsModel::setSfxVolume).withValue(settingsModel.getSfxVolume()));

        addDrawable(new Text("music volume: ", Position.centeredX(80).withAnchor(Anchor.TOP_RIGHT)));
        addMouseComponent(new Slider(drawingContextProvider, Position.centeredX(80)).withOnValueChanged(settingsModel::setMusicVolume).withValue(settingsModel.getMusicVolume()));

        addMouseComponent(new TextButton("apply", Position.centeredX(110), drawingContextProvider).withAction(settingsController::save));

        addMouseComponent(new TextButton("back", Position.centeredX(120), drawingContextProvider).withAction(settingsController::back));
    }
}
