package com.mad.fight.menus.settings;

import com.mad.fight.GameSettings;
import com.mad.fight.ScreenViewFactory;
import com.mad.fight.ScreenViewSelector;
import com.mad.fight.SettingsManager;
import com.mad.fight.menus.AbstractMenuController;

public class SettingsController extends AbstractMenuController {
    private final SettingsModel settingsModel;

    public SettingsController(SettingsModel settingsModel, ScreenViewSelector screenViewSelector, ScreenViewFactory screenViewFactory) {
        super(screenViewSelector, screenViewFactory);
        this.settingsModel = settingsModel;
    }

    void back() {
        this.screenViewSelector.setCurrentScreen(screenViewFactory.createMainMenuView());
    }

    void save() {
        SettingsManager.INSTANCE.saveSettings(GameSettings.fromSettingsModel(settingsModel));
    }
}
