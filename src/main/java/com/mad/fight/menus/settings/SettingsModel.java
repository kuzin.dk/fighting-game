package com.mad.fight.menus.settings;

import lombok.Data;

@Data
public class SettingsModel {
    private int scalingFactor;
    private int sfxVolume;
    private int musicVolume;
}
