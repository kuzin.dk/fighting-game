package com.mad.fight.menus.mainmenu;

import com.mad.fight.MusicManager;
import com.mad.fight.ScreenViewFactory;
import com.mad.fight.ScreenViewSelector;
import com.mad.fight.SoundEffectsManager;
import com.mad.fight.menus.AbstractMenuController;

public class MainMenuController extends AbstractMenuController {
    public MainMenuController(ScreenViewSelector screenViewSelector, ScreenViewFactory screenViewFactory) {
        super(screenViewSelector, screenViewFactory);
        MusicManager.INSTANCE.startMusic(MusicManager.MAIN_THEME);
        SoundEffectsManager.INSTANCE.playSoundEffect(SoundEffectsManager.vTitle);
    }

    void startGame() {
        MusicManager.INSTANCE.stopMusic();
        screenViewSelector.setCurrentScreen(screenViewFactory.createPrefightView());
    }

    void goToSettings() {
        this.screenViewSelector.setCurrentScreen(screenViewFactory.createSettingsView());
    }

    void quit() {
        System.exit(0);
    }
}
