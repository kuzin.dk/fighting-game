package com.mad.fight.menus.lobby;

import com.mad.fight.*;
import com.mad.fight.net.Command;
import lombok.Setter;

public class LobbyController {
    private final GameLifecycleManager gameLifecycleManager;

    @Setter
    private LobbyView lobbyView;

    public LobbyController(GameLifecycleManager gameLifecycleManager, ScreenViewSelector screenViewSelector, ScreenViewFactory screenViewFactory) {
        this.gameLifecycleManager = gameLifecycleManager;
        this.gameLifecycleManager.connect();
        this.gameLifecycleManager.startMatchmaking();

        this.gameLifecycleManager.getClientLobbyHandler().subscribeToLobbyUpdates(event -> {
            Command command = event.getCommand();

            // These updates are not executed on the UI thread, therefore UI changes must be made with Platform.invokeLater()
            if (command.equals(Command.MATCH_BRIEF)) {
                if (lobbyView != null) {
                    lobbyView.layoutForPlayerNumber(event.getEnvironment().getPlayerNumber());
                    SoundEffectsManager.INSTANCE.playSoundEffect(SoundEffectsManager.click);
                    lobbyView.showReadyButton();
                }
            } else if (command.equals(Command.OPPONENT_READY)) {
                if (lobbyView != null) {
                    lobbyView.showOpponentReadyMarker();
                    SoundEffectsManager.INSTANCE.playSoundEffect(SoundEffectsManager.click);
                }
            } else if (command.equals(Command.OPPONENT_UNREADY)) {
                if (lobbyView != null) {
                    lobbyView.hideOpponentReadyMarker();
                    SoundEffectsManager.INSTANCE.playSoundEffect(SoundEffectsManager.click);
                }
            } else if (command.equals(Command.PREPARE_FOR_MATCH_START)) {
                screenViewSelector.setCurrentScreen(screenViewFactory.createFightScreen());
                MusicManager.INSTANCE.stopMusic();
            }
        });


        MusicManager.INSTANCE.startMusic(MusicManager.CHARACTER_SELECT);
        SoundEffectsManager.INSTANCE.playSoundEffect(SoundEffectsManager.vCharacterSelect);
    }

    void ready() {
        System.out.println("Clicked Ready");

        this.gameLifecycleManager.getClientLobbyHandler().selectCharacter(1);
        this.gameLifecycleManager.getClientLobbyHandler().ready();

        this.lobbyView.showPlayerReadyMarker();
        this.lobbyView.hideReadyButton();
        this.lobbyView.showUnreadyButton();
    }

    void unready() {
        System.out.println("Clicked Unready");

        this.gameLifecycleManager.getClientLobbyHandler().unready();

        this.lobbyView.hidePlayerReadyMarker();
        this.lobbyView.showReadyButton();
        this.lobbyView.hideUnreadyButton();
    }

    public void disconnect() {
        this.gameLifecycleManager.disconnect();
    }
}
