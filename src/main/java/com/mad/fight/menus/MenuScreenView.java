package com.mad.fight.menus;

import com.mad.fight.Drawable;
import com.mad.fight.ScreenView;
import com.mad.fight.Updatable;
import com.mad.fight.components.JavaFXEventDelegator;
import com.mad.fight.components.MouseClickListener;
import com.mad.fight.components.MouseComponent;
import com.mad.fight.components.MouseMoveListener;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * This class provides a convenient base implementation of a menu as a ScreenView, where multiple
 * components can be registered easily, and the events from the canvas are delegated
 * to the components automatically.
 */
public abstract class MenuScreenView implements ScreenView {
    private final JavaFXEventDelegator<MouseEvent> mouseClickDelegator = new JavaFXEventDelegator<>();
    private final JavaFXEventDelegator<MouseEvent> mouseMoveDelegator = new JavaFXEventDelegator<>();
    private final JavaFXEventDelegator<KeyEvent> keyPressedDelegator = new JavaFXEventDelegator<>();

    private final List<Updatable> updatables = new ArrayList<>();
    private final List<Drawable> drawables = new ArrayList<>();

    public void onSelected() {
        // NOOP
    }

    @Override
    public void setHandlers(Canvas canvas) {
        canvas.setOnMouseMoved(mouseMoveDelegator);
        canvas.setOnMouseClicked(mouseClickDelegator);
        canvas.setOnKeyPressed(keyPressedDelegator);
    }

    @Override
    public void clearHandlers(Canvas canvas) {
        canvas.setOnMouseMoved(null);
        canvas.setOnMouseClicked(null);
        canvas.setOnKeyPressed(null);
    }

    protected void addMouseComponent(MouseComponent mouseComponent) {
        if (mouseComponent instanceof MouseMoveListener) {
            mouseMoveDelegator.addHandler(((MouseMoveListener) mouseComponent).getMouseMoveListener());
        }

        if (mouseComponent instanceof MouseClickListener) {
            mouseClickDelegator.addHandler(((MouseClickListener) mouseComponent).getMouseClickListener());
        }

        addDrawable(mouseComponent);
    }

    protected void removeMouseComponent(MouseComponent mouseComponent) {
        if (mouseComponent instanceof MouseMoveListener) {
            mouseMoveDelegator.removeHandler(((MouseMoveListener) mouseComponent).getMouseMoveListener());
        }

        if (mouseComponent instanceof MouseClickListener) {
            mouseClickDelegator.removeHandler(((MouseClickListener) mouseComponent).getMouseClickListener());
        }

        removeDrawable(mouseComponent);
    }

    protected void clear() {
        updatables.clear();
        drawables.clear();
        mouseClickDelegator.clear();
        mouseMoveDelegator.clear();
    }

    protected void clearAll(Collection<Drawable> drawables) {
        this.drawables.removeAll(drawables);
    }

    protected void addDrawable(Drawable drawable) {
        drawables.add(drawable);
    }

    protected void addDrawable(Drawable drawable, int index) {
        drawables.add(index, drawable);
    }

    protected void removeDrawable(Drawable drawable) {
        drawables.remove(drawable);
    }

    protected void addUpdatable(Updatable updatable) {
        updatables.add(updatable);
    }

    public List<Drawable> getDrawables() {
        return drawables;
    }
    public List<Updatable> getUpdatables() {
        return updatables;
    }
}
