package com.mad.fight.menus;

import com.mad.fight.ScreenViewFactory;
import com.mad.fight.ScreenViewSelector;

public abstract class AbstractMenuController {
    protected final ScreenViewSelector screenViewSelector;
    protected final ScreenViewFactory screenViewFactory;

    public AbstractMenuController(ScreenViewSelector screenViewSelector, ScreenViewFactory screenViewFactory) {
        this.screenViewSelector = screenViewSelector;
        this.screenViewFactory = screenViewFactory;
    }
}
