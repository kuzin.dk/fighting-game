package com.mad.fight;

import com.mad.fight.menus.settings.SettingsModel;
import lombok.Data;

import java.io.Serializable;

@Data
public class GameSettings implements Serializable {
    private int scalingFactor = 3;
    private double musicVolume = 1;
    private double sfxVolume = 1;

    public static GameSettings fromSettingsModel(SettingsModel model) {
        GameSettings gameSettings = new GameSettings();
        gameSettings.setScalingFactor(model.getScalingFactor());
        gameSettings.setMusicVolume((double)model.getMusicVolume() / 10);
        gameSettings.setSfxVolume((double)model.getSfxVolume() / 10);
        return gameSettings;
    }

    public SettingsModel toSettingsModel() {
        SettingsModel settingsModel = new SettingsModel();
        settingsModel.setScalingFactor(scalingFactor);
        settingsModel.setMusicVolume((int) (musicVolume * 10));
        settingsModel.setSfxVolume((int) (sfxVolume * 10));
        return settingsModel;
    }
}