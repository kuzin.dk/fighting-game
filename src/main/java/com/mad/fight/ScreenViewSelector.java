package com.mad.fight;

import javafx.scene.canvas.Canvas;

import java.util.ArrayList;
import java.util.List;

/**
 * A ScreenViewSelector provides a means to manage and switch between
 * views, ensuring that they are all properly usable and visible when they
 * are selected. This means that any handlers will be registered, and the
 * proper updatables and drawables will be updated and drawn respectively.
 */
public class ScreenViewSelector {
    private final Canvas canvas;

    private ScreenView currentScreen;
    private ScreenView overlayScreen;

    public ScreenViewSelector(Canvas canvas) {
        this.canvas = canvas;
    }

    /**
     * Switches to the specified screen
     *
     * @param screen The screen to switch to
     */
    public void setCurrentScreen(ScreenView screen) {
        if (this.currentScreen != null) {
            this.currentScreen.clearHandlers(canvas);
        }

        this.currentScreen = screen;

        if (this.currentScreen != null) {
            this.currentScreen.setHandlers(canvas);
            this.currentScreen.onSelected();
        }
    }

    /**
     * Overlays a screen by replacing the updatables of the current screen, but adding the drawables.
     * In effect, you can interact with and see the overlay, but the current screen is still below.
     *
     * @param screen The screen to use as an overlay
     */
    public void setOverlayScreen(ScreenView screen) {
        if (this.overlayScreen != null) {
            this.overlayScreen.clearHandlers(canvas);
        }

        this.overlayScreen = screen;

        if (this.overlayScreen != null) {
            this.overlayScreen.setHandlers(canvas);
            this.overlayScreen.onSelected();
        }
    }

    /**
     * Removes the current overlay screen, and restores the functionality of the current screen
     */
    public void removeOverlayScreen() {
        if (this.overlayScreen != null) {
            this.overlayScreen.clearHandlers(canvas);
        }

        this.overlayScreen = null;

        if (this.currentScreen != null) {
            this.currentScreen.setHandlers(canvas);
            this.currentScreen.onSelected();
        }
    }

    public List<Drawable> getDrawables() {
        List<Drawable> drawables = new ArrayList<>();

        if (currentScreen != null) {
            drawables.addAll(currentScreen.getDrawables());
        }

        if (overlayScreen != null) {
            drawables.addAll(overlayScreen.getDrawables());
        }

        return drawables;
    }

    public List<Updatable> getUpdatables() {
        if (overlayScreen != null) {
            return overlayScreen.getUpdatables();
        }

        if (currentScreen == null) {
            return new ArrayList<>();
        }

        return currentScreen.getUpdatables();
    }
}
