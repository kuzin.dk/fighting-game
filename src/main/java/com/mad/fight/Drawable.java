package com.mad.fight;

import javafx.scene.canvas.GraphicsContext;

public interface Drawable {
    /**
     * Any class that implements this interface may be drawn on a Canvas.
     * The fill color of the provided GraphicsContext must be set to Color.BLACK if
     * changed during the render.
     * @param gc An instance of a GraphicsContext that may be used to draw with
     * @param updateCount The number of logical updates at the time of the rendering
     */
    void draw(GraphicsContext gc, long updateCount, DrawingContext dc);
}
