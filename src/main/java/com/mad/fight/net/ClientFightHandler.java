package com.mad.fight.net;

import com.mad.fight.GameLifecycleManager;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.jspace.SequentialSpace;
import org.jspace.Space;

import java.awt.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.mad.fight.net.InteractionWrapper.ff;
import static com.mad.fight.net.InteractionWrapper.getSingleField;

@Log4j2
public class ClientFightHandler {
    private final InteractionWrapper interactionWrapper;

    @Getter
    private final Space fightSpace = new SequentialSpace();

    private final ExecutorService executorService = Executors.newSingleThreadExecutor();

    @Getter
    private final FightContext fightContext = new FightContext();

    public ClientFightHandler(InteractionWrapper interactionWrapper, ClientLobbyHandler.LobbyContext lobbyContext) {
        this.interactionWrapper = interactionWrapper;

        this.fightContext.lobbyId = lobbyContext.getLobbyId();
        this.fightContext.opponentCharacter = lobbyContext.getOpponentCharacter();
        this.fightContext.playerNumber = lobbyContext.getPlayerNumber();
    }

    public void start(Runnable fightStartedAction, Runnable fightEndAction) {
        executorService.execute(() -> handleFight(fightStartedAction, fightEndAction));
    }

    @SneakyThrows(InterruptedException.class)
    private void handleFight(Runnable fightStartedAction, Runnable fightEndAction) {
        handleFightStart(fightStartedAction);

        boolean gameActive = true;

        while (gameActive) {
            interactionWrapper.send(Command.INPUT_CODE_UPDATE, fightContext.playerInput, fightContext.playerHit, fightContext.playerDirectionRight);
            fightContext.playerInput = 0;

            if (fightContext.playerHit) fightContext.playerHit = false;

            boolean continueToFullStateUpdate = getSingleField(interactionWrapper.await(Command.CONTINUE_FULL_STATE_UPDATE, ff(Boolean.class)));

            if (continueToFullStateUpdate) {
                interactionWrapper.send(Command.POSITION_UPDATE, fightContext.playerX, fightContext.playerY, fightContext.playerVelX, fightContext.playerVelY);
                fightContext.time = getSingleField(interactionWrapper.await(Command.TIME_UPDATE, ff(Integer.class)));
                Object[] opponentPosArr = interactionWrapper.await(Command.OPPONENT_POSITION_UPDATE, ff(Integer.class), ff(Integer.class), ff(Integer.class), ff(Integer.class));
                fightContext.opponentX = (int) opponentPosArr[1];
                fightContext.opponentY = (int) opponentPosArr[2];
                fightContext.opponentVelX = (int) opponentPosArr[3];
                fightContext.opponentVelY = (int) opponentPosArr[4];

                fightSpace.put(Command.OPPONENT_POSITION_UPDATE.getCode(), fightContext.opponentX, fightContext.opponentY, fightContext.opponentVelX, fightContext.opponentVelY);

                fightContext.opponentHealth = getSingleField(interactionWrapper.await(Command.OPPONENT_HEALTH_UPDATE, ff(Integer.class)));
            }

            Object[] minimalUpdateArr = interactionWrapper.await(Command.MINIMAL_STATE_UPDATE, ff(Integer.class), ff(Integer.class), ff(Boolean.class));
            fightContext.opponentInput = (int) minimalUpdateArr[1];
            fightContext.playerHealth = (int) minimalUpdateArr[2];
            gameActive = (boolean) minimalUpdateArr[3];

            fightSpace.put(Command.MINIMAL_STATE_UPDATE.getCode(), fightContext.opponentInput, fightContext.playerHealth);
        }

        fightContext.matchWinner = getSingleField(interactionWrapper.await(Command.MATCH_WINNER, ff(Integer.class)));
        fightEndAction.run();
    }

    private void handleFightStart(Runnable fightStartedAction) throws InterruptedException {
        log.info("Awaiting player and opponent positions...");

        Object[] initialPlayerPos = interactionWrapper.await(Command.POSITION_UPDATE, ff(Integer.class), ff(Integer.class));
        fightContext.playerX = (int) initialPlayerPos[1];
        fightContext.playerY = (int) initialPlayerPos[2];

        Object[] initialOpponentPos = interactionWrapper.await(Command.OPPONENT_POSITION_UPDATE, ff(Integer.class), ff(Integer.class));
        fightContext.opponentX = (int) initialOpponentPos[1];
        fightContext.opponentY = (int) initialOpponentPos[2];

        log.info("Positions received, ready for match start");

        interactionWrapper.send(Command.READY_FOR_MATCH_START);
        interactionWrapper.await(Command.MATCH_START);

        fightStartedAction.run();
        log.info("Match started");
    }


    @Getter
    public static class FightContext {
        private String lobbyId;
        private GameLifecycleManager.PlayerNumber playerNumber;

        private int time = 99;
        private int opponentCharacter;

        private int playerX;
        private int playerY;
        private int playerVelX;
        private int playerVelY;

        @Setter
        private int playerInput;
        private int playerHealth = 100;

        @Setter
        private boolean playerDirectionRight;
        @Setter
        private boolean playerHit;

        private int opponentX;
        private int opponentY;
        private int opponentInput;
        private int opponentVelX;
        private int opponentVelY;
        private int opponentHealth = 100;

        @Getter
        private int matchWinner = -1;

        public void updatePlayerPosition(Point pos, Point vel) {
            playerX = pos.x;
            playerY = pos.y;
            playerVelX = vel.x;
            playerVelY = vel.y;
        }
    }
}
