package com.mad.fight.net;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.jspace.Space;

@RequiredArgsConstructor
@Log4j2
public class OutgoingChannel {
    private final Space space;

    public void send(Command command, Object... fields) throws InterruptedException {
        Object[] sendFields = new Object[fields.length + 1];
        System.arraycopy(fields, 0, sendFields, 1, fields.length);
        sendFields[0] = command.getCode();

        if (command.isRequireBriefing()) {
            this.space.put(command.getCode());
            log.debug("Sent briefing for command " + command);
        }

        if (command.getSignature().length == 1) {
            return;
        }

        this.space.put(sendFields);
        log.debug("Sent command " + command);
    }
}
