package com.mad.fight.net;

import com.mad.fight.GameLifecycleManager;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.jspace.SpaceRepository;

import java.awt.*;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static com.mad.fight.net.InteractionWrapper.*;

@Log4j2
public class Server {
    private static final String BASE_URI;

    static {
        Properties properties = new Properties();

        try {
            properties.load(GameLifecycleManager.class.getClassLoader().getResourceAsStream("application.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        BASE_URI = "tcp://" + properties.getOrDefault("host", "localhost:8080");
        log.info("Using host " + BASE_URI);
    }

    public static void main(String[] args) {
        Server server = new Server();
        server.acceptMatchmakingRequests();
    }

    private static final int FULL_UPDATE_INTERVAL = 5;

    private final List<Thread> games = new ArrayList<>();
    private final SpaceRepository repository = new SpaceRepository();

    private final InteractionWrapper matchmakingInteractionWrapper = InteractionWrapper.createAndAddSpaces("ps", "sp", repository);

    private boolean running = false;

    private Server() {
        repository.addGate( BASE_URI + "/?keep");
    }


    @SneakyThrows(InterruptedException.class)
    private void acceptMatchmakingRequests() {
        running = true;

        while (running) {
            performMatchmaking();
        }
    }

    private String awaitPlayerConnection() throws InterruptedException {
        // Await player to join
        String playerId = getSingleField(matchmakingInteractionWrapper.await(Command.CONNECTION_REQUEST, ff(String.class)));
        log.info("Player '" + playerId + "' requested to join");

        // Send acknowledgement to player
        matchmakingInteractionWrapper.send(Command.CONNECTION_REQUEST, playerId);
        log.info("Player '" + playerId + "' has successfully connected");

        return playerId;
    }

    private void performMatchmaking() throws InterruptedException {
        log.info("Matchmaking started. Awaiting two players to connect");
        String player1Id = awaitPlayerConnection();
        log.info("Awaiting one player");
        String player2Id = awaitPlayerConnection();
        log.info("Two players have connected. Creating lobby");

        Thread t = new Thread(() -> lobby(player1Id, player2Id));

        games.add(t);
        t.start();
    }

    @SneakyThrows({InterruptedException.class, ExecutionException.class})
    private void lobby(String player1Id, String player2Id) {
        String lobbyId = UUID.randomUUID().toString();

        InteractionWrapper p1InteractionWrapper = InteractionWrapper.createAndAddSpaces(player1Id + "s", "s" + player1Id, repository);
        InteractionWrapper p2InteractionWrapper = InteractionWrapper.createAndAddSpaces(player2Id + "s", "s" + player2Id, repository);

        // Send to the lobby that the match channel has been created
        // These must be addressed to ensure that the correct players receive the redirect command
        matchmakingInteractionWrapper.send(Command.LOBBY_REDIRECT, player1Id, lobbyId);
        matchmakingInteractionWrapper.send(Command.LOBBY_REDIRECT, player2Id, lobbyId);

        // Match briefing (tells players whether they are player 1 or player 2)
        p1InteractionWrapper.send(Command.MATCH_BRIEF, 1);
        p2InteractionWrapper.send(Command.MATCH_BRIEF, 2);

        log.info("Lobby " + lobbyId + " has been created");

        // Await character selecting
        ExecutorService executorService = Executors.newFixedThreadPool(2);

        Future<?> p1CharacterSelection = executorService.submit(new PlayerLobbyHandler(p1InteractionWrapper, p2InteractionWrapper, player1Id));
        Future<?> p2CharacterSelection = executorService.submit(new PlayerLobbyHandler(p2InteractionWrapper, p1InteractionWrapper, player2Id));

        p1CharacterSelection.get();
        p2CharacterSelection.get();

        log.info("Both players are ready. Starting match: " + player1Id + " vs " + player2Id);

        Thread.sleep(2000);

        //Start the game
        p1InteractionWrapper.send(Command.PREPARE_FOR_MATCH_START);
        p2InteractionWrapper.send(Command.PREPARE_FOR_MATCH_START);

        fight(p1InteractionWrapper, p2InteractionWrapper, player1Id, player2Id);
    }

    @SneakyThrows(InterruptedException.class)
    private void fight(InteractionWrapper player1InteractionWrapper, InteractionWrapper player2InteractionWrapper, String player1Id, String player2Id) {
        Point player1Pos = new Point(75, 0), player2Pos = new Point(75 + 160, 0);
        Point player1Velocity, player2Velocity;
        int player1InputCode = 0, player2InputCode = 0;
        int player1Health = 100, player2Health = 100;

        int time = 99;

        player1InteractionWrapper.send(Command.POSITION_UPDATE, player1Pos.x, player1Pos.y);
        player1InteractionWrapper.send(Command.OPPONENT_POSITION_UPDATE, player2Pos.x, player2Pos.y);

        player2InteractionWrapper.send(Command.POSITION_UPDATE, player2Pos.x, player2Pos.y);
        player2InteractionWrapper.send(Command.OPPONENT_POSITION_UPDATE, player1Pos.x, player1Pos.y);

        player1InteractionWrapper.await(Command.READY_FOR_MATCH_START);
        player2InteractionWrapper.await(Command.READY_FOR_MATCH_START);

        player1InteractionWrapper.send(Command.MATCH_START);
        player2InteractionWrapper.send(Command.MATCH_START);

        // Wait for countdown
        Thread.sleep(3000);
        Instant startTime = Instant.now();

        boolean gameActive = true;

        while (gameActive) {
            for (int i = 0; i < FULL_UPDATE_INTERVAL && gameActive; i++) {
                boolean player1Hit, player1DirectionRight, player2Hit, player2DirectionRight;

                Object[] player1InputUpdate = player1InteractionWrapper.await(Command.INPUT_CODE_UPDATE, ff(Integer.class), ff(Boolean.class), ff(Boolean.class));
                player1InputCode = (int) player1InputUpdate[1];
                player1Hit = (boolean) player1InputUpdate[2];
                player1DirectionRight = (boolean) player1InputUpdate[3];

                Object[] player2InputUpdate = player2InteractionWrapper.await(Command.INPUT_CODE_UPDATE, ff(Integer.class), ff(Boolean.class), ff(Boolean.class));
                player2InputCode = (int) player2InputUpdate[1];
                player2Hit = (boolean) player2InputUpdate[2];
                player2DirectionRight = (boolean) player2InputUpdate[3];

                if (i == FULL_UPDATE_INTERVAL - 1 || player1Hit || player2Hit) {
                    player1InteractionWrapper.send(Command.CONTINUE_FULL_STATE_UPDATE, true);
                    player2InteractionWrapper.send(Command.CONTINUE_FULL_STATE_UPDATE, true);

                    Object[] player1PosArr = player1InteractionWrapper.await(Command.POSITION_UPDATE, ff(Integer.class), ff(Integer.class), ff(Integer.class), ff(Integer.class));
                    player1Pos = getPositionFromObjectArr(player1PosArr);
                    player1Velocity = getVelFromObjectArr(player1PosArr);

                    Object[] player2PosArr = player2InteractionWrapper.await(Command.POSITION_UPDATE, ff(Integer.class), ff(Integer.class), ff(Integer.class), ff(Integer.class));
                    player2Pos = getPositionFromObjectArr(player2PosArr);
                    player2Velocity = getVelFromObjectArr(player2PosArr);

                    // Hit detection - determines whether players are hit, and forces the correct movement for the client.
                    if (player1Hit && !player2Hit) {
                        Point posDiff = new Point(player1Pos.x - player2Pos.x, player1Pos.y - player2Pos.y);

                        if (!player1DirectionRight && posDiff.x > 0 && posDiff.x < 50) {
                            player2Health -= 10;
                            player2Velocity.x = -5;
                            player2Velocity.y = -5;
                        } else if (player1DirectionRight && posDiff.x < 0 && posDiff.x > -50) {
                            player2Health -= 10;
                            player2Velocity.x = 5;
                            player2Velocity.y = -5;
                        }
                    } else if (player2Hit && !player1Hit) {
                        Point posDiff = new Point(player2Pos.x - player1Pos.x, player2Pos.y - player1Pos.y);

                        if (!player2DirectionRight && posDiff.x > 0 && posDiff.x < 50) {
                            player1Health -= 10;
                            player1Velocity.x = -5;
                            player1Velocity.y = -5;
                        } else if (player2DirectionRight && posDiff.x < 0 && posDiff.x > -50) {
                            player1Health -= 10;
                            player1Velocity.x = 5;
                            player1Velocity.y = -5;
                        }
                    }

                    time = 99 - (int) Duration.between(startTime, Instant.now()).getSeconds();

                    player1InteractionWrapper.send(Command.TIME_UPDATE, time);
                    player2InteractionWrapper.send(Command.TIME_UPDATE, time);

                    player1InteractionWrapper.send(Command.OPPONENT_POSITION_UPDATE, player2Pos.x, player2Pos.y, player2Velocity.x, player2Velocity.y);
                    player2InteractionWrapper.send(Command.OPPONENT_POSITION_UPDATE, player1Pos.x, player1Pos.y, player1Velocity.x, player1Velocity.y);

                    player1InteractionWrapper.send(Command.OPPONENT_HEALTH_UPDATE, player2Health);
                    player2InteractionWrapper.send(Command.OPPONENT_HEALTH_UPDATE, player1Health);
                } else {
                    player1InteractionWrapper.send(Command.CONTINUE_FULL_STATE_UPDATE, false);
                    player2InteractionWrapper.send(Command.CONTINUE_FULL_STATE_UPDATE, false);
                }

                if (player1Health == 0 || player2Health == 0 || time <= 0) {
                    gameActive = false;
                }

                player1InteractionWrapper.send(Command.MINIMAL_STATE_UPDATE, player2InputCode, player1Health, gameActive);
                player2InteractionWrapper.send(Command.MINIMAL_STATE_UPDATE, player1InputCode, player2Health, gameActive);

                Thread.sleep(200);
            }
        }

        if (player1Health == 0) {
            player1InteractionWrapper.send(Command.MATCH_WINNER, 2);
            player2InteractionWrapper.send(Command.MATCH_WINNER, 2);
        } else if (player2Health == 0) {
            player1InteractionWrapper.send(Command.MATCH_WINNER, 1);
            player2InteractionWrapper.send(Command.MATCH_WINNER, 1);
        } else {
            player1InteractionWrapper.send(Command.MATCH_WINNER, 0);
            player2InteractionWrapper.send(Command.MATCH_WINNER, 0);
        }

        removeSpaces(player1Id + "s", "s" + player1Id, repository);
        removeSpaces(player2Id + "s", "s" + player2Id, repository);
    }

    private Point getPositionFromObjectArr(Object[] arr) {
        return new Point((int) arr[1], (int) arr[2]);
    }

    private Point getVelFromObjectArr(Object[] arr) {
        return new Point((int) arr[3], (int) arr[4]);
    }

    @RequiredArgsConstructor
    private static class PlayerLobbyHandler implements Runnable {
        private final InteractionWrapper playerInteractionWrapper;
        private final InteractionWrapper opponentInteractionWrapper;
        private final String playerId;

        @Override
        @SneakyThrows(InterruptedException.class)
        public void run() {
            boolean awaitStart = false;

            boolean ready = false;
            int selection = 0;

            while (!awaitStart) {
                IncomingChannel.ReceivedCommand receivedCommand = playerInteractionWrapper.awaitAnyCommand();
                Command command = receivedCommand.getCommand();

                if (command.equals(Command.SELECT_CHARACTER)) {
                    if (ready) {
                        throw new IllegalStateException("The player cannot select a character while they are ready");
                    }

                    selection = getSingleField(receivedCommand.getArgs());
                    opponentInteractionWrapper.send(Command.OPPONENT_CHARACTER_SELECTED, selection);
                    log.info(playerId + " selected character " + selection);
                } else if (command.equals(Command.READY)) {
                    ready = true;
                    log.info(playerId + " is ready.");
                    opponentInteractionWrapper.send(Command.OPPONENT_READY);
                } else if (command.equals(Command.UNREADY)) {
                    ready = false;
                    log.info(playerId + " is no longer ready.");
                    opponentInteractionWrapper.send(Command.OPPONENT_UNREADY);
                } else if (command.equals(Command.BOTH_PLAYERS_READY)) {
                    awaitStart = true;
                    playerInteractionWrapper.send(Command.STOP_LOBBY_UPDATES);
                }
            }
        }
    }
}
