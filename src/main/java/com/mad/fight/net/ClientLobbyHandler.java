package com.mad.fight.net;

import com.mad.fight.ConsumerEventDelegator;
import com.mad.fight.GameLifecycleManager;
import lombok.Data;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

import static com.mad.fight.net.InteractionWrapper.*;

@Log4j2
public class ClientLobbyHandler {
    private final String playerId;
    private final InteractionWrapper matchmakingInteractionWrapper;
    private InteractionWrapper directInteractionWrapper;

    public ClientLobbyHandler(String playerId, InteractionWrapper matchmakingInteractionWrapper, InteractionWrapper directInteractionWrapper) {
        this.playerId = playerId;
        this.matchmakingInteractionWrapper = matchmakingInteractionWrapper;
        this.directInteractionWrapper = directInteractionWrapper;
    }

    private final ConsumerEventDelegator<LobbyEvent> lobbyEventDelegator = new ConsumerEventDelegator<>();
    private final ExecutorService executorService = Executors.newSingleThreadExecutor();

    @Getter
    private volatile LobbyContext env = new LobbyContext();

    public void start() {
        env = new LobbyContext();
        executorService.submit(this::handleMatchmakingAndLobby);
    }

    @SneakyThrows(InterruptedException.class)
    public void selectCharacter(int character) {
        if (env.playerReady) {
            throw new IllegalStateException("A character may not be selected with ready");
        }

        env.playerCharacter = character;
        directInteractionWrapper.send(Command.SELECT_CHARACTER, character);
    }

    @SneakyThrows(InterruptedException.class)
    public void ready() {
        if (env.state != State.IN_LOBBY) {
            throw new IllegalStateException("Expected state to be " + State.IN_LOBBY);
        }

        env.playerReady = true;
        directInteractionWrapper.send(Command.READY);
        log.info("Player is ready");

        if (env.opponentReady) {
            env.listeningForLobbyUpdates = false;
            directInteractionWrapper.send(Command.BOTH_PLAYERS_READY);
            env.state = State.AWAITING_MATCH_START;
        }
    }

    @SneakyThrows(InterruptedException.class)
    public void unready() {
        if (env.state != State.IN_LOBBY) {
            throw new IllegalStateException("Expected state to be " + State.IN_LOBBY);
        }

        env.playerReady = false;
        directInteractionWrapper.send(Command.UNREADY);
        log.info("Player is no longer ready");
    }

    public void subscribeToLobbyUpdates(Consumer<LobbyEvent> consumer) {
        this.lobbyEventDelegator.addHandler(consumer);
    }

    @SneakyThrows(InterruptedException.class)
    private void handleMatchmakingAndLobby() {
        joinMatchmaking();
        awaitLobbyRedirect();
        awaitMatchBriefing();

        // From here we don't quite know which order things will happen in until both players are ready and the
        // AWAITING_MATCH_START command is sent.
        listenForLobbyUpdates();
        log.info("Lobby update listener ended");

        awaitMatchStart();
    }

    private void joinMatchmaking() throws InterruptedException {
        matchmakingInteractionWrapper.send(Command.CONNECTION_REQUEST, playerId);
        log.info("Sent connection request");

        //Get response that you've connected successfully
        matchmakingInteractionWrapper.await(Command.CONNECTION_REQUEST, af(playerId));
        log.info("Connection request was granted");

        handleEvent(Command.CONNECTION_REQUEST);

        env.state = State.MATCHMAKING;
        log.info("Matchmaking begun, waiting for opponent...");
    }

    private void awaitLobbyRedirect() throws InterruptedException {
        env.lobbyId = getSingleField(matchmakingInteractionWrapper.await(Command.LOBBY_REDIRECT, af(playerId), ff(String.class)));
        env.state = State.IN_LOBBY;
        handleEvent(Command.LOBBY_REDIRECT);
    }

    private void awaitMatchBriefing() throws InterruptedException {
        int playerNumber = getSingleField(directInteractionWrapper.await(Command.MATCH_BRIEF, ff(Integer.class)));
        log.info("Received match briefing: Player: " + playerNumber);
        env.playerNumber = GameLifecycleManager.PlayerNumber.fromInt(playerNumber);
        handleEvent(Command.MATCH_BRIEF);
    }

    private void listenForLobbyUpdates() throws InterruptedException {
        env.listeningForLobbyUpdates = true;

        log.info("Lobby update listener started");

        while (env.listeningForLobbyUpdates) {
            IncomingChannel.ReceivedCommand receivedCommand = directInteractionWrapper.awaitAnyCommand();
            Command command = receivedCommand.getCommand();
            log.debug("Lobby update listener received command " + command.toString());

            if (command.equals(Command.OPPONENT_CHARACTER_SELECTED)) {
                env.opponentCharacter = getSingleField(receivedCommand.getArgs());
                handleEvent(Command.OPPONENT_CHARACTER_SELECTED);
            } else if (command.equals(Command.OPPONENT_READY)) {
                env.opponentReady = true;
                handleEvent(Command.OPPONENT_READY);

                if (env.playerReady) {
                    directInteractionWrapper.send(Command.BOTH_PLAYERS_READY);
                    env.state = State.AWAITING_MATCH_START;
                    env.listeningForLobbyUpdates = false;
                }
            } else if (command.equals(Command.OPPONENT_UNREADY)) {
                env.opponentReady = false;
                handleEvent(Command.OPPONENT_UNREADY);
            } else if (command.equals(Command.STOP_LOBBY_UPDATES)) {
                env.listeningForLobbyUpdates = false;
            }
        }
    }

    private void awaitMatchStart() throws InterruptedException {
        log.info("Awaiting match start");
        directInteractionWrapper.await(Command.PREPARE_FOR_MATCH_START);
        env.state = State.MATCH_STARTED;
        handleEvent(Command.PREPARE_FOR_MATCH_START);
        log.info("Match started");
    }

    private void handleEvent(Command command) {
        this.lobbyEventDelegator.handle(new LobbyEvent(command, env));
    }

    public void disconnect() throws InterruptedException {
    }

    @Getter
    public static class LobbyContext {
        private boolean listeningForLobbyUpdates;

        private State state;
        private String lobbyId;
        private GameLifecycleManager.PlayerNumber playerNumber;

        private int playerCharacter;
        private boolean playerReady;

        private int opponentCharacter;
        private boolean opponentReady;
    }

    public enum State {
        MATCHMAKING,
        IN_LOBBY,
        AWAITING_MATCH_START,
        MATCH_STARTED
    }

    @Data
    public static class LobbyEvent {
        private final Command command;
        private final LobbyContext environment;
    }
}
