package com.mad.fight.net;

import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.jspace.Space;
import org.jspace.TemplateField;

import static com.mad.fight.net.InteractionWrapper.af;
import static com.mad.fight.net.InteractionWrapper.ff;

@RequiredArgsConstructor
@Log4j2
public class IncomingChannel {
    @Getter
    private final Space space;

    Object[] await(Command command, TemplateField... fields) throws InterruptedException {
        TemplateField[] templateFields = new TemplateField[fields.length + 1];
        System.arraycopy(fields, 0, templateFields, 1, fields.length);
        templateFields[0] = af(command.getCode());

        if (command.isRequireBriefing()) {
            // Await pre-message briefing
            this.space.get(af(command.getCode()));
            log.debug("Received briefing for command " + command);
        }

        if (command.getSignature().length == 1) {
            return command.getSignature();
        }

        Object[] arr = this.space.get(templateFields);
        log.debug("Received command " + command);
        return arr;
    }

    /**
     * Since .get() must be called with the right amount of expected elements, each command contains the expected formal
     * fields so that we can support awaiting any command with a variable number of elements. When such a field is to be
     * received, the briefing of the command must be received first, so that it is known which command to expect.
     */
    ReceivedCommand awaitAnyCommand() throws InterruptedException {
        Object[] arr = this.space.get(ff(Integer.class));

        // TODO: JSpace swallows the Exceptions and returns null when something goes wrong, therefore this null check
        // TODO: The bug must be fixed in JSpace if we need to get the right exception
        if (arr == null) {
            throw new InterruptedException("The await was either cancelled or an error occurred");
        }

        int commandCode = (int) arr[0];

        Command command = Command.fromCode(commandCode).orElseThrow(() -> new IllegalStateException("Received unrecognized command with code: " + commandCode));
        log.debug("Received briefing for command " + command);

        if (command.getSignature().length == 1) {
            return new ReceivedCommand(command, command.getSignature());
        }

        Object[] args = this.space.get(command.getSignature());
        log.debug("Received command " + command);

        return new ReceivedCommand(command, args);
    }

    @Data
    static class ReceivedCommand {
        private final Command command;
        private final Object[] args;
    }
}
