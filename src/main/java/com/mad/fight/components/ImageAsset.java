package com.mad.fight.components;

import com.mad.fight.Drawable;
import com.mad.fight.DrawingContext;
import com.mad.fight.PixelArtRenderer;
import javafx.scene.canvas.GraphicsContext;
import lombok.Setter;

import java.awt.*;

public class ImageAsset implements Drawable {
    private javafx.scene.image.Image image;

    @Setter
    private Position position;
    private boolean mirrored;

    public ImageAsset(javafx.scene.image.Image image, Position position) {
        this.image = image;
        this.position = position;
    }

    public ImageAsset mirrored() {
        this.mirrored = true;
        return this;
    }

    private Rectangle getBounds(DrawingContext dc) {
        Point offset = Anchor.anchorOffset(position.getAnchor(),
                new Dimension(image.widthProperty().intValue(), image.heightProperty().intValue()));

        return new Rectangle(
                position.getX(dc) + offset.x,
                position.getY(dc) + offset.y,
                image.widthProperty().intValue(),
                image.heightProperty().intValue());
    }

    @Override
    public void draw(GraphicsContext gc, long updateCount, DrawingContext dc) {
        Rectangle bounds = getBounds(dc);
        PixelArtRenderer.render(image, new Point(bounds.x, bounds.y), mirrored, new PixelArtRenderer.Crop(), dc, gc);
    }
}
