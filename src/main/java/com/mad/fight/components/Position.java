package com.mad.fight.components;

import com.mad.fight.DrawingContext;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Data
@AllArgsConstructor
public class Position {
    private Anchor anchor;
    private boolean centeredX;
    private boolean centeredY;

    @Getter(AccessLevel.NONE)
    private int x;

    @Getter(AccessLevel.NONE)
    private int y;

    public int getX(DrawingContext drawingContext) {
        if(centeredX) {
            return drawingContext.getWidth() / 2;
        }

        return x;
    }

    public void setX(int x) {
        this.x = x;
        this.centeredX = false;
    }

    public int getY(DrawingContext drawingContext) {
        if(centeredY) {
            return drawingContext.getHeight() / 2;
        }

        return y;
    }

    public void setY(int y) {
        this.y = y;
        this.centeredY = false;
    }

    public Position withAnchor(Anchor anchor) {
        this.anchor = anchor;
        return this;
    }

    public static Position centeredX(int y) {
        return new Position(Anchor.TOP_CENTER, true, false, 0, y);
    }
}
