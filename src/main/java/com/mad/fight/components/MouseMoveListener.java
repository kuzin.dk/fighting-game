package com.mad.fight.components;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

/**
 * This interface will indicate that the inheritor has a MouseMoveListener (EventHandler with MouseEvent)
 * generic type, that can be registered
 */
public interface MouseMoveListener extends MouseListener {
    EventHandler<MouseEvent> getMouseMoveListener();
}
