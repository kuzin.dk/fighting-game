package com.mad.fight.components;

import com.mad.fight.Drawable;
import com.mad.fight.DrawingContext;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import lombok.Setter;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class Text implements Drawable {
    private static final Map<Character, Image> TYPEFACE;
    private static final int TYPEFACE_HEIGHT = 8;
    private static int SPACE_WIDTH = 4;


    static {
        TYPEFACE = new HashMap<>();

        for (char i = 'a'; i <= 'z'; i++) {
            TYPEFACE.put(i, new Image("png/alphabet/" + i + ".png"));
            TYPEFACE.put(Character.toUpperCase(i), new Image("png/alphabet/" + i + ".png"));
        }

        for (char i = '0'; i <= '9'; i++) {
            TYPEFACE.put(i, new Image("png/alphabet/" + i + ".png"));
        }

        TYPEFACE.put(':', new Image("png/alphabet/colon.png"));
    }

    private String text;
    private int width;

    @Setter
    private Position position;

    public Text(String text, Position position) {
        setText(text);
        this.position = position;
    }

    private Image getCharImage(char c) {
        return TYPEFACE.getOrDefault(c, TYPEFACE.get('x'));
    }

    public void setText(String text) {
        this.text = text;

        this.width = 0;

        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);

            if (c == ' ') {
                width += SPACE_WIDTH;
            } else {
                width += getCharImage(c).widthProperty().intValue() + 1;
            }
        }
    }

    protected Rectangle getBounds(DrawingContext context) {
        Point offset = Anchor.anchorOffset(position.getAnchor(), new Dimension(width, TYPEFACE_HEIGHT));
        return new Rectangle(this.position.getX(context) + offset.x, this.position.getY(context) + offset.y, this.width, TYPEFACE_HEIGHT);
    }

    protected javafx.scene.paint.Color getColor() {
        return javafx.scene.paint.Color.web("#ffffff");
    }

    @Override
    public void draw(GraphicsContext gc, long updateCount, DrawingContext dc) {
        Rectangle bounds = getBounds(dc);
        int pixelPosX = bounds.x * dc.getScalingFactor();
        int pixelPosY = bounds.y * dc.getScalingFactor();

        int xPixelOffset = 0;

        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);

            if (c == ' ') {
                xPixelOffset += dc.getScalingFactor() * SPACE_WIDTH;
            } else {
                xPixelOffset += drawCharacter(c, gc, pixelPosX + xPixelOffset, pixelPosY, dc);
            }
        }
    }

    private int drawCharacter(char c, GraphicsContext gc, int x, int y, DrawingContext dc) {
        Image charImg = getCharImage(c);
        PixelReader reader = charImg.getPixelReader();

        javafx.scene.paint.Color color = getColor();

        for (int xc = 0; xc < charImg.widthProperty().intValue(); xc++) {
            for (int yc = 0; yc < charImg.heightProperty().intValue(); yc++) {
                javafx.scene.paint.Color pixelColor = reader.getColor(xc, yc);

                if (pixelColor.equals(javafx.scene.paint.Color.web("#ffffff"))) {
                    gc.setFill(color);
                } else {
                    gc.setFill(pixelColor);
                }

                gc.fillRect(x + xc * dc.getScalingFactor(), y + yc * dc.getScalingFactor(), dc.getScalingFactor(), dc.getScalingFactor());
            }
        }

        return ((int) charImg.getWidth()) * dc.getScalingFactor() + dc.getScalingFactor();
    }
}
