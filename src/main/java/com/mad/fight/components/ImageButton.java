package com.mad.fight.components;

import com.mad.fight.*;
import javafx.event.EventHandler;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;

import java.awt.*;

public class ImageButton implements Drawable, MouseListener, MouseMoveListener, MouseClickListener {
    private final DrawingContextProvider drawingContextProvider;

    private Image mouseoverImage;
    private final Image image;
    private Position position;
    private boolean mouseOverActive = false;

    private Runnable clickAction = () -> {
    };

    private final EventHandler<MouseEvent> mouseMoveListener;

    private final EventHandler<MouseEvent> mouseClickListener;

    public ImageButton(Image image, Position position, DrawingContextProvider drawingContextProvider) {
        this.image = image;
        this.position = position;
        this.drawingContextProvider = drawingContextProvider;

        mouseMoveListener = event -> {
            Rectangle bounds = getBounds();
            int sf = drawingContextProvider.getDrawingContext().getScalingFactor();
            Rectangle scaledBounds = new Rectangle(bounds.x * sf, bounds.y * sf, bounds.width * sf, bounds.height * sf);

            if (scaledBounds.contains(event.getX(), event.getY())) {
                if (!mouseOverActive) {
                    SoundEffectsManager.INSTANCE.playSoundEffect(SoundEffectsManager.mouseover);
                }

                mouseOverActive = true;
            } else {
                mouseOverActive = false;
            }
        };


        mouseClickListener = event -> {
            Rectangle bounds = getBounds();
            int sf = drawingContextProvider.getDrawingContext().getScalingFactor();
            Rectangle scaledBounds = new Rectangle(bounds.x * sf, bounds.y * sf, bounds.width * sf, bounds.height * sf);

            if (scaledBounds.contains(event.getX(), event.getY())) {
                clickAction.run();
                SoundEffectsManager.INSTANCE.playSoundEffect(SoundEffectsManager.click);
            }
        };
    }

    public ImageButton withMouseoverImage(Image mouseoverImage) {
        this.mouseoverImage = mouseoverImage;
        return this;
    }

    public ImageButton withClickAction(Runnable clickAction) {
        this.clickAction = clickAction;
        return this;
    }

    private Rectangle getBounds() {
        DrawingContext dc = drawingContextProvider.getDrawingContext();
        Point offset = Anchor.anchorOffset(position.getAnchor(), new Dimension(image.widthProperty().intValue(), image.heightProperty().intValue()));
        return new Rectangle(
                position.getX(dc) + offset.x,
                position.getY(dc) + offset.y,
                image.widthProperty().intValue(),
                image.heightProperty().intValue());
    }

    @Override
    public EventHandler<MouseEvent> getMouseClickListener() {
        return mouseClickListener;
    }

    @Override
    public EventHandler<MouseEvent> getMouseMoveListener() {
        return mouseMoveListener;
    }

    @Override
    public void draw(GraphicsContext gc, long updateCount, DrawingContext dc) {
        Rectangle bounds = getBounds();

        if (mouseOverActive && mouseoverImage != null) {
            PixelArtRenderer.render(mouseoverImage, new Point(bounds.x, bounds.y), dc, gc);
        } else {
            PixelArtRenderer.render(image, new Point(bounds.x, bounds.y), dc, gc);
        }
    }
}
