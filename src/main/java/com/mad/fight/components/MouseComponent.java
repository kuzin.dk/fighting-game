package com.mad.fight.components;

import com.mad.fight.Drawable;

public interface MouseComponent extends MouseListener, Drawable {
}
