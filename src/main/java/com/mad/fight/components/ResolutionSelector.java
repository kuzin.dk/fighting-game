package com.mad.fight.components;

import com.google.common.collect.Lists;
import com.mad.fight.Drawable;
import com.mad.fight.DrawingContext;
import com.mad.fight.DrawingContextProvider;
import javafx.event.EventHandler;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;

import java.awt.*;
import java.util.List;
import java.util.function.Consumer;

public class ResolutionSelector implements Drawable, MouseComponent, MouseMoveListener, MouseClickListener {
    private static final List<Integer> SCALING_FACTORS = Lists.newArrayList(5, 6, 7, 8, 9);

    private static final Image LEFT_BTN = new Image("png/settings/leftbtn.png");
    private static final Image LEFT_BTN_MO = new Image("png/settings/leftbtnc.png");
    private static final Image RIGHT_BTN = new Image("png/settings/rghtbtn.png");
    private static final Image RIGHT_BTN_MO = new Image("png/settings/rghtbtnc.png");

    private final DrawingContextProvider drawingContextProvider;

    private final ImageButton rightButton;
    private final ImageButton leftButton;

    private Position position;
    private final Text resText;
    private int selectedScalingFactorIndex;

    private final EventHandler<MouseEvent> clickAction;
    private final EventHandler<MouseEvent> moveAction;

    private final Consumer<Integer> onScalingFactorUpdated;

    public ResolutionSelector(Position position, int value, Consumer<Integer> onScalingFactorUpdated, DrawingContextProvider drawingContextProvider) {
        this.selectedScalingFactorIndex = SCALING_FACTORS.indexOf(value);
        if (selectedScalingFactorIndex == -1) selectedScalingFactorIndex = 0;

        this.onScalingFactorUpdated = onScalingFactorUpdated;
        this.drawingContextProvider = drawingContextProvider;
        this.position = position;
        DrawingContext dc = drawingContextProvider.getDrawingContext();

        Rectangle bounds = getBounds();

        resText = new Text(getText(), new Position(Anchor.TOP_CENTER, false, false, bounds.x + bounds.width / 2, bounds.y));

        Position downButtonPos = new Position(
                Anchor.TOP_LEFT,
                false,
                false,
                bounds.x,
                bounds.y);

        this.leftButton = new ImageButton(LEFT_BTN, downButtonPos, drawingContextProvider)
                .withMouseoverImage(LEFT_BTN_MO)
                .withClickAction(() -> {
                    if (selectedScalingFactorIndex > 0) {
                        selectedScalingFactorIndex--;
                        onScalingFactorUpdated.accept(SCALING_FACTORS.get(selectedScalingFactorIndex));
                        resText.setText(getText());
                    }
                });

        Position upButtonPos = new Position(
                Anchor.TOP_RIGHT,
                false,
                false,
                bounds.x + bounds.width,
                position.getY(dc));

        this.rightButton = new ImageButton(RIGHT_BTN, upButtonPos, drawingContextProvider)
                .withMouseoverImage(RIGHT_BTN_MO)
                .withClickAction(() -> {
                    if (selectedScalingFactorIndex < SCALING_FACTORS.size() - 1) {
                        selectedScalingFactorIndex++;
                        onScalingFactorUpdated.accept(SCALING_FACTORS.get(selectedScalingFactorIndex));
                        resText.setText(getText());
                    }
                });

        clickAction = event -> {
            leftButton.getMouseClickListener().handle(event);
            rightButton.getMouseClickListener().handle(event);
        };

        moveAction = event -> {
            leftButton.getMouseMoveListener().handle(event);
            rightButton.getMouseMoveListener().handle(event);
        };
    }

    private String getText() {
        DrawingContext dc = drawingContextProvider.getDrawingContext();
        int width = dc.getWidth();
        int height = dc.getHeight();

        int sf = SCALING_FACTORS.get(selectedScalingFactorIndex);
        return width * sf + "x" + height * sf;
    }

    private Rectangle getBounds() {
        DrawingContext dc = drawingContextProvider.getDrawingContext();
        Point offset = Anchor.anchorOffset(position.getAnchor(), new Dimension(60, 7));
        return new Rectangle(position.getX(dc) + offset.x, position.getY(dc) + offset.y, 60, 7);
    }

    @Override
    public void draw(GraphicsContext gc, long updateCount, DrawingContext dc) {
        resText.draw(gc, updateCount, dc);
        leftButton.draw(gc, updateCount, dc);
        rightButton.draw(gc, updateCount, dc);
    }

    @Override
    public EventHandler<MouseEvent> getMouseClickListener() {
        return clickAction;
    }

    @Override
    public EventHandler<MouseEvent> getMouseMoveListener() {
        return moveAction;
    }
}
