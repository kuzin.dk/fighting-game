package com.mad.fight;

import com.mad.fight.fight.FightScreen;
import com.mad.fight.menus.IntroScreen;
import com.mad.fight.menus.lobby.LobbyController;
import com.mad.fight.menus.lobby.LobbyView;
import com.mad.fight.menus.mainmenu.MainMenuController;
import com.mad.fight.menus.mainmenu.MainMenuView;
import com.mad.fight.menus.pause.PauseController;
import com.mad.fight.menus.pause.PauseView;
import com.mad.fight.menus.settings.SettingsController;
import com.mad.fight.menus.settings.SettingsModel;
import com.mad.fight.menus.settings.SettingsView;

/**
 * This class is responsible for creating all views, and provides
 * views with everything they need to switch views.
 */
public class ScreenViewFactory {
    private final DrawingContextProvider drawingContextProvider;
    private final ScreenViewSelector screenViewSelector;
    private final GameLifecycleManager gameLifecycleManager;

    ScreenViewFactory(DrawingContextProvider drawingContextProvider, ScreenViewSelector screenViewSelector, GameLifecycleManager gameLifecycleManager) {
        this.drawingContextProvider = drawingContextProvider;
        this.screenViewSelector = screenViewSelector;
        this.gameLifecycleManager = gameLifecycleManager;
    }

    public IntroScreen createIntroScreen() {
        return new IntroScreen(this, screenViewSelector);
    }

    public MainMenuView createMainMenuView() {
        MainMenuController mainMenuController = new MainMenuController(screenViewSelector, this);
        return new MainMenuView(mainMenuController, drawingContextProvider);
    }

    public PauseView createPauseView() {
        PauseController pauseController = new PauseController(screenViewSelector, this);
        return new PauseView(pauseController, drawingContextProvider);
    }

    public SettingsView createSettingsView() {
        SettingsModel settingsModel = SettingsManager.INSTANCE.getSettings().toSettingsModel();
        return new SettingsView(new SettingsController(settingsModel, screenViewSelector, this), settingsModel, drawingContextProvider);
    }

    public LobbyView createPrefightView() {
        LobbyController controller = new LobbyController(gameLifecycleManager, screenViewSelector, this);
        LobbyView view = new LobbyView(controller, drawingContextProvider);
        controller.setLobbyView(view);
        return view;
    }

    public FightScreen createFightScreen() {
        return new FightScreen(gameLifecycleManager, screenViewSelector, this);
    }
}
