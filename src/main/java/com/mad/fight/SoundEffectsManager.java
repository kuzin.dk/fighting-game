package com.mad.fight;

import javafx.scene.media.AudioClip;

import java.net.URL;

public class SoundEffectsManager {
    public static final SoundEffectsManager INSTANCE = new SoundEffectsManager();

    public static final AudioClip click = new AudioClip(resolveUrl("/sfx/click.wav"));
    public static final AudioClip mouseover = new AudioClip(resolveUrl("/sfx/mouseover.wav"));
    public static final AudioClip intro = new AudioClip(resolveUrl("/music/intro.wav"));


    public static final AudioClip punch = new AudioClip(resolveUrl("/sfx/punch.wav"));
    public static final AudioClip swing = new AudioClip(resolveUrl("/sfx/swing.wav"));


    public static final AudioClip vTitle = new AudioClip(resolveUrl("/sfx/voice/v_title.wav"));
    public static final AudioClip vFight = new AudioClip(resolveUrl("/sfx/voice/v_fight.wav"));
    public static final AudioClip vBoris = new AudioClip(resolveUrl("/sfx/voice/v_boris.wav"));
    public static final AudioClip vVs = new AudioClip(resolveUrl("/sfx/voice/v_vs.wav"));
    public static final AudioClip vCharacterSelect = new AudioClip(resolveUrl("/sfx/voice/v_character_select.wav"));

    public static final AudioClip vPlayer1Wins = new AudioClip(resolveUrl("/sfx/voice/v_player1_wins.wav"));
    public static final AudioClip vPlayer2Wins = new AudioClip(resolveUrl("/sfx/voice/v_player2_wins.wav"));
    public static final AudioClip vTimeUp = new AudioClip(resolveUrl("/sfx/voice/v_time_up.wav"));

    private static String resolveUrl(String resource) {
        URL url = SoundEffectsManager.class.getResource(resource);

        if(url == null) {
            throw new IllegalStateException("Resource '" + resource + "' could not be found.");
        }

        return url.toString();
    }

    private SoundEffectsManager() {

    }

    public void playSoundEffect(AudioClip audioClip) {
        audioClip.play(SettingsManager.INSTANCE.getSettings().getSfxVolume());
    }
}
