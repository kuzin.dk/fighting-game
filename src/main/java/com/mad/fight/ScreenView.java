package com.mad.fight;

import com.mad.fight.Drawable;
import com.mad.fight.Updatable;
import javafx.scene.canvas.Canvas;

import java.util.List;

/**
 * When implemented, a ScreenView is compatible with a ScreenViewSelector
 * and can be managed in such a way that all the proper handlers, drawables,
 * and updatables will be set, drawn, and updated respectively.
 */
public interface ScreenView {
    void onSelected();

    void setHandlers(Canvas canvas);

    void clearHandlers(Canvas canvas);

    List<Drawable> getDrawables();

    List<Updatable> getUpdatables();
}
