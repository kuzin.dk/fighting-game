package com.mad.fight.fight;

import com.mad.fight.*;
import com.mad.fight.net.ClientFightHandler;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import lombok.Getter;
import lombok.Setter;

import java.awt.*;

import static com.mad.fight.fight.Animation.Frame.f;

public class Player implements Drawable, Updatable {
    static final Animation BORIS_IDLE = new Animation(
            true,
            f(new Image("png/character/boris_idle_1.png"), 10, 0, 0),
            f(new Image("png/character/boris_idle_2.png"), 10, 0, 0),
            f(new Image("png/character/boris_idle_3.png"), 10, 0, 0)
    );

    private static final Image shadow = new Image("png/character/shadow1.png");

    private final AnimationHandler animationHandler = new AnimationHandler(BORIS_IDLE);

    private final KeyHandler keyHandler;
    private final Viewport viewport;
    private final GameLifecycleManager gameLifecycleManager;

    @Getter
    private final PlayerMovementHandler movementHandler;

    @Setter
    private boolean transmitPosition;

    Player(KeyHandler keyHandler, Viewport viewport, GameLifecycleManager gameLifecycleManager) {
        this.keyHandler = keyHandler;
        this.viewport = viewport;
        this.gameLifecycleManager = gameLifecycleManager;
        movementHandler = new PlayerMovementHandler(keyHandler, animationHandler);
    }

    @Override
    public void draw(GraphicsContext gc, long updateCount, DrawingContext dc) {
        animationHandler.draw(dc, gc, viewport.offset(movementHandler.getPos()), !movementHandler.isDirectionRight());
    }

    @Override
    public void update(long updateCount) {
        movementHandler.update(updateCount);

        if (transmitPosition) {
            ClientFightHandler.FightContext fightContext = gameLifecycleManager.getClientFightHandler().getFightContext();
            fightContext.updatePlayerPosition(movementHandler.getPos(), movementHandler.getVel());
            fightContext.setPlayerInput(fightContext.getPlayerInput() | this.keyHandler.getKeysCode());
            fightContext.setPlayerDirectionRight(this.movementHandler.isDirectionRight());

            if (this.movementHandler.isHit()) {
                fightContext.setPlayerHit(true);
            }
        }
    }
}
