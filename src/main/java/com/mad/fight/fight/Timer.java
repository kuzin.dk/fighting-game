package com.mad.fight.fight;

import com.google.common.collect.Lists;
import com.mad.fight.*;
import com.mad.fight.components.Anchor;
import com.mad.fight.components.Position;
import com.mad.fight.components.Text;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

import java.awt.*;
import java.util.List;

public class Timer implements Drawable, Updatable {
    private static final List<Image> countdownImages = Lists.newArrayList(
            new Image("png/map/countdown3.png"),
            new Image("png/map/countdown2.png"),
            new Image("png/map/countdown1.png"),
            new Image("png/map/countdownfight.png")
    );

    private final GameLifecycleManager gameLifecycleManager;

    private int count = 0;
    private int time = 99;
    private int countdownIndex = -1;
    private boolean countdownDone = false;
    private Position position = new Position(Anchor.TOP_CENTER, true, false, 0, 40);
    private Text text = new Text(Integer.toString(time), new Position(Anchor.TOP_CENTER, true, false, 0, 6));

    public Timer(GameLifecycleManager gameLifecycleManager) {
        this.gameLifecycleManager = gameLifecycleManager;
    }

    @Override
    public void draw(GraphicsContext gc, long updateCount, DrawingContext dc) {
        if (countdownIndex < 4 && countdownIndex >= 0) {
            Image countdownImage = countdownImages.get(countdownIndex);
            Point offset = Anchor.anchorOffset(position.getAnchor(), new Dimension(countdownImage.widthProperty().intValue(), countdownImage.heightProperty().intValue()));
            PixelArtRenderer.render(countdownImage, new Point(position.getX(dc) + offset.x, position.getY(dc) + offset.y), dc, gc);
        }

        if (time < 10) {
            text.setText("0" + time);
        } else {
            text.setText(Integer.toString(time));
        }

        text.draw(gc, updateCount, dc);
    }

    @Override
    public void update(long updateCount) {
        time = gameLifecycleManager.getClientFightHandler().getFightContext().getTime();

        if (count % 60 == 0) {
            countdownIndex++;
        }

        if (countdownIndex == 4) {
            countdownDone = true;
        }

        count++;
    }
}
