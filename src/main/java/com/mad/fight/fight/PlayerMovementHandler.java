package com.mad.fight.fight;

import com.mad.fight.SoundEffectsManager;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import lombok.Getter;
import lombok.Setter;

import java.awt.*;

import static com.mad.fight.fight.Animation.Frame.f;

public class PlayerMovementHandler {
    private static final Animation BORIS_JUMP = new Animation(
            true,
            f(new Image("png/character/boris_jump_2.png"), 10, 0, 0)
    );

    private static final Animation BORIS_WALK = new Animation(
            true,
            f(new Image("png/character/boris_walk_1.png"), 8, 0, 0),
            f(new Image("png/character/boris_walk_2.png"), 8, 0, 0),
            f(new Image("png/character/boris_walk_3.png"), 8, 0, 0),
            f(new Image("png/character/boris_walk_4.png"), 8, -5, 0)
    );

    private static final Animation BORIS_PUNCH = new Animation(
            false,
            f(new Image("png/character/boris_punch_1.png"), 4, 0, 0),
            f(new Image("png/character/boris_punch_2.png"), 4, 0, 0),
            f(new Image("png/character/boris_punch_3.png"), 20, 0, 0)
    );

    private static final Animation BORIS_JUMP_PUNCH = new Animation(
            true,
            f(new Image("png/character/boris_jump_punch.png"), 10, 0, 0)
    );

    @Getter
    @Setter
    private Point pos = new Point(20, 55);

    @Getter
    @Setter
    private Point vel = new Point();

    private final KeyHandler keyHandler;
    private final AnimationHandler animationHandler;

    @Setter
    private PlayerMovementHandler opponent;

    private int punchCooldown;
    private int jumpCooldown;
    private boolean takeDamage;

    @Getter
    private boolean directionRight;

    @Getter
    private boolean hit;

    public PlayerMovementHandler(KeyHandler keyHandler, AnimationHandler animationHandler) {
        this.keyHandler = keyHandler;
        this.animationHandler = animationHandler;
    }

    private void takeDamage() {
        takeDamage = true;
    }

    private void doTakeDamage() {
        takeDamage = false;
        vel.y = -4;

        if (directionRight) {
            vel.x = -5;
        } else {
            vel.x = 5;
        }
    }

    private boolean canMove() {
        if (punchCooldown > 0) {
            return false;
        }

        return true;
    }

    private boolean canMoveLeft() {
        if (!canMove()) {
            return false;
        }

        return true;
    }

    private boolean canMoveRight() {
        if (!canMove()) {
            return false;
        }

        return true;
    }

    private int getOpponentCollisionBoost(Point newPos) {
        int posDiff = newPos.x - opponent.getPos().x;

        if (posDiff >= -20 && posDiff <= 20 && newPos.y == opponent.getPos().y) {
            if (posDiff > 0) {
                return 1;
            } else {
                return -1;
            }
        }

        return 0;
    }

    private int getOutOfBoundsBoost(Point newPos) {
        if ((newPos.x - opponent.getPos().x > 160) || newPos.x >= 320) {
            return -1;
        } else if ((opponent.getPos().x - newPos.x > 160) || newPos.x <= -120) {
            return 1;
        }

        return 0;
    }

    public void update(long updateCount) {
        hit = false;

        directionRight = opponent.getPos().x > pos.x;

        //y coordinate for ground
        int ground = 40;

        //Jump
        if (pos.y < ground) {
            this.vel.y += 1;
        } else {
            this.vel.y = 0;
            animationHandler.cancelAnimation(BORIS_JUMP);
            animationHandler.cancelAnimation(BORIS_JUMP_PUNCH);
        }


        // Punch
        if (keyHandler.isKeyDown(KeyCode.Z) && punchCooldown == 0) {
            if (pos.y < ground) {
                animationHandler.setCurrentAnimation(BORIS_JUMP_PUNCH);
            } else {
                animationHandler.setCurrentAnimation(BORIS_PUNCH);
            }

            punchCooldown = 30;
            int xPosDiff = opponent.getPos().x - pos.x;

            if (directionRight) {
                if (vel.x < 4 && pos.y == ground) {
                    vel.x = 4;

                    if (xPosDiff > 10 && xPosDiff < 50) {
                        opponent.takeDamage();
                        hit = true;
                        SoundEffectsManager.INSTANCE.playSoundEffect(SoundEffectsManager.punch);
                    } else {
                        SoundEffectsManager.INSTANCE.playSoundEffect(SoundEffectsManager.swing);
                    }
                }
            } else {
                if (vel.x > -4 && pos.y == ground) {
                    vel.x = -4;

                    if (xPosDiff < -10 && xPosDiff > -50) {
                        opponent.takeDamage();
                        hit = true;
                        SoundEffectsManager.INSTANCE.playSoundEffect(SoundEffectsManager.punch);
                    } else {
                        SoundEffectsManager.INSTANCE.playSoundEffect(SoundEffectsManager.swing);
                    }
                }
            }
        }

        //Left right movement
        if (keyHandler.isKeyDown(KeyCode.LEFT) && canMoveLeft()) {
            if (pos.y == ground) {
                animationHandler.setCurrentAnimation(BORIS_WALK);
                animationHandler.setReverseAnimation(true);
            }

            if (vel.x > -1) {
                vel.x -= 1;
            }
        } else if (keyHandler.isKeyDown(KeyCode.RIGHT) && canMoveRight()) {
            if (pos.y == ground) {
                animationHandler.setCurrentAnimation(BORIS_WALK);
            }

            if (vel.x < 1) {
                vel.x += 1;
            }
        } else {
            animationHandler.cancelAnimation(BORIS_WALK);
        }


        //Jump
        if (keyHandler.isKeyDown(KeyCode.SPACE) && vel.y == 0 && pos.y == ground && jumpCooldown == 0) {
            vel.y = -12;
            jumpCooldown = 35;
            animationHandler.setCurrentAnimation(BORIS_JUMP);

            if (keyHandler.isKeyDown(KeyCode.LEFT)) {
                vel.x -= 1;
            } else if (keyHandler.isKeyDown(KeyCode.RIGHT)) {
                vel.x += 1;
            }
        }

        if (takeDamage) {
            doTakeDamage();
        }

        int clampedVelX = Integer.compare(vel.x, 0);

        if (this.pos.y < ground || vel.y != 0) {
            // In the air x vel is divided by 2 every 30 updates (low friction)
            if (updateCount % 30 == 0) {
                vel.x -= vel.x / 10 + clampedVelX;
            }
        } else {
            // On the ground x vel is divided by 2 every 4 updates (high friction)
            if (updateCount % 4 == 0) {
                vel.x -= vel.x / 10 + clampedVelX;
            }
        }

        Point newPos = new Point(this.pos);
        newPos.x += vel.x;
        newPos.y += vel.y;


        // Bounds control
        int outOfBoundsBoost = getOutOfBoundsBoost(newPos);

        if (Math.abs(outOfBoundsBoost) > 0) {
            vel.x = 0;
            newPos.x += outOfBoundsBoost;
        }

        int characterCollisionBoost = getOpponentCollisionBoost(newPos);

        if (Math.abs(characterCollisionBoost) > 0) {
            vel.x = 0;
            newPos.x += characterCollisionBoost;
        }

        // Rise to above ground in somehow buried underground
        if (newPos.y > ground) {
            newPos.y = ground;
            vel.y = 0;
        }

        // Update position
        this.pos = newPos;

        if (punchCooldown > 0) {
            punchCooldown--;
        }

        if (jumpCooldown > 0) {
            jumpCooldown--;
        }

        animationHandler.advanceLogic();
    }
}
