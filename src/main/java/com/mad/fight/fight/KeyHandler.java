package com.mad.fight.fight;

import com.mad.fight.components.JavaFXEventDelegator;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import lombok.Setter;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class KeyHandler {
    private static final Map<KeyCode, Integer> bitfield = new HashMap<>();

    static {
        bitfield.put(KeyCode.LEFT, 0);
        bitfield.put(KeyCode.RIGHT, 1);
        bitfield.put(KeyCode.SPACE, 2);
        bitfield.put(KeyCode.Z, 3);
    }

    private final Set<KeyCode> down = new HashSet<>();

    @Setter
    private boolean acceptInput;

    public void setHooks(JavaFXEventDelegator<KeyEvent> kpEventDelegator, JavaFXEventDelegator<KeyEvent> krEventDelegator) {
        down.clear();
        kpEventDelegator.addHandler(event -> down.add(event.getCode()));
        krEventDelegator.addHandler(event -> down.remove(event.getCode()));
    }

    public boolean isKeyDown(KeyCode keyCode) {
        if(!acceptInput) {
            return false;
        }

        return down.contains(keyCode);
    }

    public int getKeysCode() {
        int code = 0;

        for(KeyCode downKeyCode : down) {
            if(bitfield.containsKey(downKeyCode)) {
                code += 1 << bitfield.get(downKeyCode);
            }
        }

        return code;
    }

    public void setKeysState(int state) {
        down.clear();

        for(Map.Entry<KeyCode, Integer> entry : bitfield.entrySet()) {
            int entryBitFieldValue = 1 << entry.getValue();
            if(bitfield.containsKey(entry.getKey()) && (state & entryBitFieldValue) == entryBitFieldValue) {
                down.add(entry.getKey());
            }
        }
    }
}
