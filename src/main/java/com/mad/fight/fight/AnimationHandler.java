package com.mad.fight.fight;

import com.mad.fight.DrawingContext;
import com.mad.fight.PixelArtRenderer;
import javafx.scene.canvas.GraphicsContext;
import lombok.Setter;

import java.awt.*;

public class AnimationHandler {
    private int ticker;
    private int currentFrame;
    private final Animation fallbackAnimation;
    private Animation currentAnimation;

    @Setter
    private boolean reverseAnimation = false;

    public AnimationHandler(Animation fallbackAnimation) {
        this.fallbackAnimation = fallbackAnimation;
    }

    public void setCurrentAnimation(Animation animation) {
        if (animation != null && !animation.equals(currentAnimation)) {
            ticker = 0;
            currentFrame = 0;
            currentAnimation = animation;
            reverseAnimation = false;
        } else if (animation == null && currentAnimation != null) {
            ticker = 0;
            currentFrame = 0;
            currentAnimation = null;
            reverseAnimation = false;
        }
    }

    public void cancelAnimation(Animation animation) {
        if (animation.equals(currentAnimation)) {
            setCurrentAnimation(null);
        }
    }

    private Animation getActiveAnimation() {
        if (currentAnimation != null) {
            return currentAnimation;
        } else {
            return fallbackAnimation;
        }
    }

    public void advanceLogic() {
        ticker++;

        if (ticker == getActiveAnimation().getFrame(currentFrame).getDelay()) {
            ticker = 0;

            if(!reverseAnimation) {
                if (currentFrame < getActiveAnimation().getLength() - 1) {
                    // The animation is still in progress
                    currentFrame++;
                } else if (getActiveAnimation().isRepeatable()) {
                    // Repeat the animation
                    currentFrame = 0;
                } else if (getActiveAnimation().equals(currentAnimation)) {
                    // The animation is over and cannot be repeated, use the fallback animation
                    currentAnimation = null;
                }
            } else {
                if (currentFrame > 0) {
                    // The animation is still in progress
                    currentFrame--;
                } else if (getActiveAnimation().isRepeatable()) {
                    // Repeat the animation
                    currentFrame = currentAnimation.getLength() - 1;
                } else if (getActiveAnimation().equals(currentAnimation)) {
                    // The animation is over and cannot be repeated, use the fallback animation
                    currentAnimation = null;
                }
            }
        }
    }

    public void draw(DrawingContext dc, GraphicsContext gc, Point position, boolean mirrored) {
        Point offsetPos;

        if (!mirrored) {
            Animation.Frame frame = getActiveAnimation().getFrame(currentFrame);
            offsetPos = new Point(position.x + frame.getOffset().x, position.y + frame.getOffset().y);
        } else {
            offsetPos = position;
        }

        PixelArtRenderer.render(getActiveAnimation().getFrame(currentFrame).getImage(), offsetPos, mirrored, new PixelArtRenderer.Crop(), dc, gc);
    }
}
