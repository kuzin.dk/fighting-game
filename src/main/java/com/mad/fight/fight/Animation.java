package com.mad.fight.fight;

import javafx.scene.image.Image;
import lombok.Data;
import lombok.Getter;

import java.awt.*;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Animation {
    private final List<Frame> frames;

    @Getter
    private final boolean repeatable;

    public Animation(boolean repeatable, Frame... frames) {
        this.repeatable = repeatable;
        this.frames = Arrays.stream(frames).collect(Collectors.toList());
    }

    public int getLength() {
        return frames.size();
    }


    public Frame getFrame(int i) {
        return frames.get(i);
    }

    @Data
    public static class Frame {
        private final Image image;
        private final int delay;
        private final Point offset;

        public Frame(Image image, int delay, int xOffset, int yOffset) {
            this.image = image;
            this.delay = delay;
            this.offset = new Point(xOffset, yOffset);
        }

        public static Frame f(Image image, int delay, int xOffset, int yOffset) {
            return new Frame(image, delay, xOffset, yOffset);
        }
    }
}
