package com.mad.fight.fight;

import com.mad.fight.*;
import com.mad.fight.net.Command;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import lombok.Getter;
import lombok.SneakyThrows;

import java.awt.*;

import static com.mad.fight.net.InteractionWrapper.af;
import static com.mad.fight.net.InteractionWrapper.ff;

public class Opponent implements Drawable, Updatable {
    private static final Image boris = new Image("png/character/boris.png");
    private static final Image shadow = new Image("png/character/shadow1.png");

    private final KeyHandler keyHandler = new KeyHandler();
    private final Viewport viewport;
    private final GameLifecycleManager gameLifecycleManager;
    private final AnimationHandler animationHandler = new AnimationHandler(Player.BORIS_IDLE);

    @Getter
    private final PlayerMovementHandler movementHandler;

    Opponent(Viewport viewport, GameLifecycleManager gameLifecycleManager) {
        this.viewport = viewport;
        this.gameLifecycleManager = gameLifecycleManager;
        movementHandler = new PlayerMovementHandler(keyHandler, animationHandler);
        keyHandler.setAcceptInput(true);
    }

    @Override
    public void draw(GraphicsContext gc, long updateCount, DrawingContext dc) {
        animationHandler.draw(dc, gc, viewport.offset(movementHandler.getPos()), !movementHandler.isDirectionRight());
    }

    @Override
    @SneakyThrows(InterruptedException.class)
    public void update(long updateCount) {
        Object[] minimalStateUpdate = this.gameLifecycleManager.getClientFightHandler().getFightSpace().getp(af(Command.MINIMAL_STATE_UPDATE.getCode()), ff(Integer.class), ff(Integer.class));

        if (minimalStateUpdate != null) {
            this.keyHandler.setKeysState((int) minimalStateUpdate[1]);
        }

        Object[] positionUpdate = this.gameLifecycleManager.getClientFightHandler().getFightSpace().getp(af(Command.OPPONENT_POSITION_UPDATE.getCode()), ff(Integer.class), ff(Integer.class), ff(Integer.class), ff(Integer.class));

        if (positionUpdate != null) {
            movementHandler.setPos(new Point((int) positionUpdate[1], (int) positionUpdate[2]));
            movementHandler.setVel(new Point((int) positionUpdate[3], (int) positionUpdate[4]));
        }

        movementHandler.update(updateCount);
    }
}
